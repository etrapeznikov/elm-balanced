import Browser
import Html exposing ( div, input, text, kbd )
import Html.Events exposing ( onInput )
import Html.Attributes exposing ( style )

main = 
  Browser.sandbox { init = True, update = update, view = view }

type Msg 
  = InputEdited String

update : Msg -> Bool -> Bool
update msg model =
  case msg of
    InputEdited string ->
      balancedString string

view model =
  div []
    [ input [ onInput InputEdited, style "font-family" "monospace" ] []
    , text 
      ( if model
        then "Balanced"
        else "Unbalanced" ) ]


-- Solution starts

type BracketForm
  = Round
  | Curly
  | Square

type Bracket
  = Opening BracketForm
  | Closing BracketForm

parseBracket : Char -> Maybe Bracket
parseBracket char =
  case char of
    '(' -> Just ( Opening Round )
    ')' -> Just ( Closing Round )
    '[' -> Just ( Opening Square )
    ']' -> Just ( Closing Square )
    '{' -> Just ( Opening Curly )
    '}' -> Just ( Closing Curly )
    _   -> Nothing

balancedString : String -> Bool
balancedString string =
  String.toList string
    |> List.filterMap parseBracket
    |> balanced []

balanced : List BracketForm -> List Bracket -> Bool
balanced stack input =
  case ( stack, input ) of
    ( [], [] ) 
      -> True

    ( _ :: _, [] ) 
      -> False

    ( ss, Opening form :: xs ) 
      -> balanced (form :: ss) xs

    ( [], Closing form :: _ ) 
      -> False

    ( top :: ss, Closing next :: xs ) 
      -> if top == next
           then balanced ss xs
           else False
